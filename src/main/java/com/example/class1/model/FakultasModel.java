package com.example.class1.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "T_FAKULTAS")
public class FakultasModel {

	@Id
	@Column(name = "KD_FAKULTAS")
	private String kodeFakultas;
	
	@Column(name = "NM_Fakultas", nullable = false, length =25)
	private String namaFakultas;

	public String getKodeFakultas() {
		return kodeFakultas;
	}

	public void setKodeFakultas(String kodeFakultas) {
		this.kodeFakultas = kodeFakultas;
	}

	public String getNamaFakultas() {
		return namaFakultas;
	}

	public void setNamaFakultas(String namaFakultas) {
		this.namaFakultas = namaFakultas;
	}

}
