package com.example.class1.controller;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.class1.model.KotaModel;
import com.example.class1.service.KotaService;

@Controller
@RequestMapping("/karyawan")
public class KotaController {

	@Autowired
	private KotaService kotaService;	
	
	@RequestMapping("/tambah/kota")
	public String doKotaTambah(Model model) {
		this.doGenerateKode(model);
		return "/karyawan/kota-tambah";
	}
	
	@RequestMapping("/create/kota")
	public String doCreateKota (HttpServletRequest request) throws ParseException {
		
		String kodeKota = request.getParameter("kodeKota");
		String namaKota = request.getParameter("namaKota");
		
		
		KotaModel kotaModel = new KotaModel();
		kotaModel.setKodeKota(kodeKota);
		kotaModel.setNamaKota(namaKota);		
		
		this.kotaService.create(kotaModel);
		
		return "/karyawan/home";
	}
	
	public void doGenerateKode(Model model) {
		
		Integer maxId = 0;
		maxId = this.kotaService.searchMaxId();
		maxId += 1;
		
		String kodeGenerator = "KOT"+String.format("%03d",maxId);
		model.addAttribute("kodeGenerator", kodeGenerator);
	}
	
	@RequestMapping("/kota/data")
	public String doList(Model model) {
		List<KotaModel> kotaModelList = new ArrayList<KotaModel>();
		kotaModelList = this.kotaService.read();
		model.addAttribute("kotaModelList", kotaModelList);
		
		return "/karyawan/kota-list";
	}
}
