package com.example.class1.repository; //untuk menggolah query

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.class1.model.FakultasModel;

public interface FakultasRepository extends JpaRepository<FakultasModel, String> {

	@Query("SELECT F FROM FakultasModel F WHERE F.kodeFakultas =?1")
	FakultasModel searchKodeFakultas(String kodeFakultas);
	
	@Query("SELECT F FROM FakultasModel F WHERE F.namaFakultas LIKE %?1% ") // %D%(AKAN MEMUNCULKAN SEMUA YANG MENGANDUNG HURUF D)
	//A%(akan memunculkan AWALAN A) %A(akan memunculkan AKHIRAN A)
	List<FakultasModel> searchNamaFakultas(String namaFakultas);
	
	@Query("SELECT F FROM FakultasModel F WHERE F.kodeFakultas LIKE %?1%") //untuk mencari data dari 1 tabel
	List<FakultasModel> cariKodeFakultasRepository(String kodeFakultas);
}
