package com.example.class1.repository; //untuk menggolah query

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.class1.model.AnggotaModel;

public interface AnggotaRepository extends JpaRepository<AnggotaModel, String> {

	@Query("SELECT F FROM AnggotaModel F WHERE F.kodeAnggota =?1")
	AnggotaModel searchKodeAnggota(String kodeAnggota);
	
	@Query("SELECT F FROM AnggotaModel F WHERE F.namaAnggota LIKE %?1% ") // %D%(AKAN MEMUNCULKAN SEMUA YANG MENGANDUNG HURUF D)
	//A%(akan memunculkan AWALAN A) %A(akan memunculkan AKHIRAN A)
	List<AnggotaModel> searchNamaAnggota(String namaAnggota);

}
