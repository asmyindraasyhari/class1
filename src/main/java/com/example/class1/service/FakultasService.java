package com.example.class1.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.class1.model.FakultasModel;
import com.example.class1.repository.FakultasRepository;

@Service
@Transactional
public class FakultasService {

		@Autowired //untuk memanggil fakultas repository dan untuk menamai nya menjadi nama baru yang sama(huruf kecil)
		private FakultasRepository fakultasRepository;
		
		public void save (FakultasModel fakultasModel) { //untuk fungsi save
			fakultasRepository.save(fakultasModel);
		}
		
		public List<FakultasModel> read() {
			return this.fakultasRepository.findAll(); //untuk list semua yg ada di database
		}
		
		public FakultasModel searchKodeFakultas(String kodeFakultas) { 
			return this.fakultasRepository.searchKodeFakultas(kodeFakultas);
		}
		public void update (FakultasModel fakultasModel) { //untuk fungsi update
			fakultasRepository.save(fakultasModel);
		}
		public void delete (FakultasModel fakultasModel) { //untuk fungsi delete
			fakultasRepository.delete(fakultasModel);
		}
		public List<FakultasModel> searchNamaFakultas(String namaFakultas) { //untuk fungsi search
			return this.fakultasRepository.searchNamaFakultas(namaFakultas);
		}
		public List<FakultasModel> searchKodeFakultasDonk(String kodeFakultas) { //fungsi list untuk nemapilkan semua data dari FakultasModel
			return null;
		}
		// pattern dari service
		// public output  namaMethod(type input1, type inputN){}
		public void create(FakultasModel fakultasModel) {//void maksudnya jika output nya kosong
		}
		public void simpan(FakultasModel fakultasModel) {
		}
		public void insert(FakultasModel fakultasModel) {
		}
		public void mengubah(FakultasModel fakultasModel) {
		}
		public void merevisi(FakultasModel fakultasModel) {
		}
		public FakultasModel selectBintang() { //untuk menampilkan output dari FakultasModel
			return null;
		}
		public FakultasModel mencari(String namaFakultas) {
			return null;
		}
		public FakultasModel cariinDonk(String kodeFakultas, String namaFakultas) {
			return null;
		}
		public List<FakultasModel> cariKodeFakultasDonk(String kodeFakultas) {
			return this.fakultasRepository.cariKodeFakultasRepository(kodeFakultas);
		}
}